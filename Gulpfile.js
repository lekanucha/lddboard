var gulp        = require('gulp');
var browserSync = require('browser-sync');
// var compass     = require('compass');
var compass     = require('gulp-compass');
var reload      = browserSync.reload;

// Start the server
gulp.task('browser-sync', function(){
    browserSync({
        /*server: {
            baseDir: "./"
        },*/
        // proxy: "http://localhost/events/dtf2017/"
        proxy: {
            target: "http://localhost/eark/lddboard/board/client/", // can be [virtual host, sub-directory, localhost with port]
            ws: true // enables websockets
        }
    });
});

// Compile SASS & auto-inject into browsers
/*gulp.task('compass', function () {
    return gulp.src('compass/dtf17/scss/*.scss')
        .pipe(compass({
          config_file: 'compass/dtf17/config.rb',
          css: 'compass/dtf17/stylesheets',
          sass: 'compass/dtf17/scss'
        }))
        .pipe(browserSync.reload({stream:true}));
});*/

gulp.task('compass', function() {
  gulp.src(['sass/*.scss'])
    .pipe(compass({
      config_file: 'config.rb',
      css: 'board/client/stylesheets',
      sass: 'sass'
    }))
    .pipe(gulp.dest('board/client/stylesheets'))
    .pipe(browserSync.reload({
        stream:true,
        ws: true
    }));
    // .pipe(browserSync.reload);
});

// Reload all Browsers
gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('copy-folder', function() {  
  gulp.src(['aw/*.jpg', 'aw/*.png'])
    .pipe(gulp.dest('board/client/images'));
});

// Watch scss AND html files, doing different things with each.
gulp.task('default', ['browser-sync', 'compass'], function () {
    gulp.watch(['aw/*.jpg', 'aw/*.png'], ['copy-folder']);
    gulp.watch("sass/*.scss", ['compass']);
    gulp.watch(['board/client/*.html', 'board/client/*.php', 'board/client/images/*.*']).on("change", browserSync.reload);
});