<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="favicon.ico">

  <title>กระดานสนทนา</title>

  <!-- Custom styles for this template -->
  <link href="stylesheets/font-awesome.css" rel="stylesheet">
  <link href="stylesheets/jquery-ui.min.css" rel="stylesheet">
  <link href="stylesheets/bootstrap-datepicker.min.css" rel="stylesheet">
  <link href="stylesheets/jquery.jqplot.min.css" rel="stylesheet">
  <link href="stylesheets/styles.css" rel="stylesheet">

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="stylesheets/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
  <script src='https://www.google.com/recaptcha/api.js'></script>

    </head>
    <body>





      <div class="head">
        <div class="container">

          <div class="pull-left"><img id="logo" src="images/LddLogo.png"></div>
          <div>
            <div class="head-top">
              <img id="title" src="images/title_long.png">
            </div>
            <div class="head-bottom">
              <!-- <label class="control-label" for="inputSuccess4">Input with success</label> -->
              <!-- <label id="greet">สวัสดี คุณxxx</label> -->
              <div id="mainmenu" class="menu menu-left btn-group">
                <a href="template_home.php" class="btn active">
                  <span class="glyphicon glyphicon-home" aria-hidden="true"></span></a>
                  <a data-toggle="collapse" href="#collapseMenu" class="btn">เลือกห้อง</a>
                  <a href="template_post.php" class="btn">ตั้งกระทู้</a>
                  <a href="template_user.php" class="btn">เข้าระบบ/สมัครสมาชิก</a>
                  <a href="template_pr.php" class="btn">ข่าวประชาสัมพันธ์ พด.</a>
                </div>
                <!-- <div class="menu menu-right">
                  <form class="form-inline">
                    <div class="form-group has-success has-feedback">
                      <input type="text" class="form-control" placeholder="ค้นหากระทู้">
                      <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
                      <span id="inputSuccess4Status" class="sr-only">(success)</span>
                    </div>
                  </form>
                </div> -->


              </div>
            </div>

          </div>
        </div><!-- head -->


        <div class="collapse" id="collapseMenu">
          <div class="container">
            <dir id="forumMenu"></dir>

</div>
</div>
<!-- collapseMenu -->


<div class="container">
<!-- <div class="row"> -->
<div id="breadcrumb" class="xcol-xs-12"></div>
<!-- </div> -->

