<?php include 'include_head.php';?>


<div class="row">

  <div class="column-left">

    <!-- Show topic ################# -->

    <div id="box_showpost">

      <div class="topic">
        <h3><i id="topicpin" style="display:none;" class="topicpin fa fa-thumb-tack"></i>เปลือกผลไม้ที่ขึ้นราหนาเป็นก้อน เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่</h3>
        <!-- Pin button for moderator -->
        <button id="pinbutton" class="btn btn-xs">ปักหมุด</button>


        <img src="images/post/1.jpg">
        <p>เปลือกมะละกอสุก เงาะ มังคุด มีราสีขาวๆจับหนาเป็นก้อนๆ ส่งกลิ่นเหม็น น่าอาเจียน
          เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่ ใส่แล้วจะเป็นการเพาะเชื้อราไม่พึงประสงค์ด้วยหรือเปล่า</p>
          <p>ทั้งนี้ใน พด.2 ประกอบด้วยจุลินทรีย์ 5 สายพันธุ์ได้แก่<br>
            Pichia membranifaciens<br>
            Lactobacillus fermentum<br>
            Bacillus megaterium<br>
            Bacillus subtilis<br>
            Burkholderia unamae</p>
            <p>กลัวว่าใส่ลงไปแล้ว จะได้สายพันธุ์อื่น ที่เป็นภัยต่อมนุษย์ ทั้งทางตรงและทางอ้อม
            </p>
            <p class="postfooter">TAG: <a href="#">ความรู้เกษตร</a><br>
              POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</p>
            </div>
            <div class="reply">
              <span class="repno">
                <i id="reppin1" style="display:none;" class="reppin fa fa-thumb-tack"></i> ความคิดเห็นที่ 1
                <button class="reppinbutton btn btn-xs" data-target="#reppin1">ปักหมุด</button>
              </span>

              <img src="images/post/4.jpg">

              <p>ไม่ได้เอามากินก็ใส่ๆ ไปเถอะ แบคทีเรียหนาแน่นขนาดนั้น 
                ราไม่เหลือ ถ้าไม่มีความชื้นกับอากาศ

                ทำเองหมักเอง เคยโยนรา โยนเน่าลงไป มันก็ออกมาใช้ได้เหมือนกัน</p>
                <p class="postfooter">POST: สมาชิกหมายเลข <span class="moderator">7432854938</span> UPDATE: 15/06/2560</p>
              </div>
              <div class="reply">
                <span class="repno">
                  <i id="reppin2" style="display:none;" class="reppin fa fa-thumb-tack"></i> ความคิดเห็นที่ 2
                  <button class="reppinbutton btn btn-xs" data-target="#reppin2">ปักหมุด</button>
                </span>

                <p>ไม่ได้เอามากินก็ใส่ๆ ไปเถอะ แบคทีเรียหนาแน่นขนาดนั้น 
                  ราไม่เหลือ ถ้าไม่มีความชื้นกับอากาศ

                  ทำเองหมักเอง เคยโยนรา โยนเน่าลงไป มันก็ออกมาใช้ได้เหมือนกัน</p>
                  <p class="postfooter">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</p>
                </div>

                <div class="reply">
                  <span class="repno">
                    <i id="reppin3" style="display:none;" class="reppin fa fa-thumb-tack"></i> ความคิดเห็นที่ 3
                    <button class="reppinbutton btn btn-xs" data-target="#reppin3">ปักหมุด</button>
                  </span>
                  <p>ที่จริงนะคะอะไรที่เริ่มขึ้นราควรทิ้งขยะหรือไม่ก็รีบฝังดิน<br>
                    จขกท.ทิ้งไว้จนส่งกลิ่น สปอร์เชื้อราปลิวไปปนเปอยู่ในอากาศ<br>
                    หายใขเข้าไปนี่อันตรายมากๆเลยนะคะ ไม่ทราบแต่อย่าใส่เลยค่ะรามันหนาขนาดนั้น<br>
                    มันเป็นพิษมากกว่านะคะ</p>
                    <p class="postfooter">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</p>
                  </div>

                  <div class="reply">
                    <span class="repno">
                      <i id="reppin4" style="display:none;" class="reppin fa fa-thumb-tack"></i> ความคิดเห็นที่ 4
                      <button class="reppinbutton btn btn-xs" data-target="#reppin4">ปักหมุด</button>
                    </span>
                    <p>ไม่มีปัญหาครับ ราพวกนั้นเขาไม่สามารถทนสภาวะในถังได้ เดี๋ยวก็สลายหมดเหลือแต่ปุ๋ยน้ำคุณภาพดีไว้ใช้ครับ
                    </p>
                    <p class="postfooter">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</p>
                  </div>

                  <div class="reply">
                    <span class="repno">
                      <i id="reppin5" style="display:none;" class="reppin fa fa-thumb-tack"></i> ความคิดเห็นที่ 5
                      <button class="reppinbutton btn btn-xs" data-target="#reppin5">ปักหมุด</button>
                    </span>
                    <p>ใส่ไปเลย  ขอให้เป็นอินทรีย์สาร    จำนำลำไยเน่าตั้งแต่สมัยทักกี้    กระทรวงเกษตรยังให้กรมพัฒนาที่ดินขนเอาไปทำปุ๋ย พ.ด. เลย</p>
                    <p class="postfooter">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</p>
                  </div>


                </div> 

                <!-- End Show topic ################# -->

                <button type="submit" class="btn btn-block space_b">แสดงเพิ่มเติม</button>

              <!-- Comment ################# -->

              <div class="form-group">
                <label for="exampleInputEmail1">แสดงความคิดเห็น</label>
                <textarea class="form-control" rows="4"></textarea>
              </div>
              <div class="row space_b">
                <div class="col-sm-12">
                <input class="btn" type="submit" value="ส่งข้อความ">
                </div>
              </div>

              <!-- End Comment ################# -->



              </div><!-- column-left -->


              <div class="column-right">
                <?php include 'include_side.php';?>
              </div><!-- column-right -->

            </div><!-- row -->



            <?php include 'include_foot.php';?>
<script>
  breadcrumb('เปลือกผลไม้ที่ขึ้นราหนาเป็นก้อน เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่เปลือกผลไม้ที่ขึ้นราหนาเป็นก้อน เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่เปลือกผลไม้ที่ขึ้นราหนาเป็นก้อน เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่เปลือกผลไม้ที่ขึ้นราหนาเป็นก้อน เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่เปลือกผลไม้ที่ขึ้นราหนาเป็นก้อน เอาใส่ลงในถังหมัก พด.2 ได้หรือไม่', ['หน้าแรก', 'ข้อมูลการใช้ที่ดิน']);
  activemenu(2);
</script>
          </body>
          </html>
