<?php include 'include_head.php';?>


<div class="row">

  <div class="column-left">

  <!-- Pin topics ################# -->
    <div id="box_pin">

      <h3 class="boxtitle">กระทู้แนะนำ Recommend Topic (Pin Post ปักหมุด)</h3>
      <p><a href="item_topic.php"><i class="fa fa-thumb-tack"></i>กรมพัฒนาที่ดิน สานต่อโครงการเกษตรทฤษฎีใหม่ เน้นส่งเสริมเกษตรกรปรับปรุงบำรุงดิน-ลดต้นทุนการผลิต</a>
          <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
          <span>TAG: <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a> <a href="#">หญ้าแฝก</a> <a href="#">โครงการพระราชดำริ</a> <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a> <a href="#">หญ้าแฝก</a> <a href="#">โครงการพระราชดำริ</a> <a href="#">อนุรักษ์ดินและน้ำ</a> </span>
      </p>
      <p>
      <a href="item_topic.php"><i class="fa fa-thumb-tack"></i>แนะแนวการจัดการดิน ช่วยเกษตรกรหลังน้ำท่วมใต้ลดลง</a>
        <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
        <span>TAG: <a href="#">ความรู้เกษตร</a></span>
      </p>
      <p>
      <a href="item_topic.php"><i class="fa fa-thumb-tack"></i>เกษตรแปลงใหญ่ให้อะไรกับเรา ทำไมภาครัฐอยากให้พวกเราทำ</a>
        <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
        <span>TAG: <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a></span>
      </p>  
    </div>  
    <!-- End Pin topics ################# -->

    <!-- Latest topics ################# -->

    <div id="box_latest">
      <h3 class="space_t2 boxtitle">กระทู้ล่าสุด</h3>

      <!-- Topic with picture -->
      <div class="row">
        <div class="img">
          <img src="images/post/1.jpg">
        </div>
        <div class="text"><a href="item_topic.php">เกษตรแปลงใหญ่ให้อะไรกับเรา ทำไมภาครัฐอยากให้พวกเราทำ</a>
          <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
          <span>TAG: <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a> <a href="#">หญ้าแฝก</a> <a href="#">โครงการพระราชดำริ</a> <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a> <a href="#">หญ้าแฝก</a> <a href="#">โครงการพระราชดำริ</a> <a href="#">อนุรักษ์ดินและน้ำ</a> </span>
        </div>
      </div>
      <!-- End Topic with picture -->
      
      <!-- Topic without picture -->
      <p>
        <a href="item_topic.php"><i class="fa fa-comments"></i>ส่งเสริมเกษตรกรทำนา "ขั้นบันได" ลดต้นทุน-เพิ่มผลผลิต
        </a>
        <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
        <span>TAG: <a href="#">ความรู้เกษตร</a></span>
      </p>
      <!-- End Topic without picture -->

      <!-- Topic with picture -->
      <div class="row">
        <div class="img">
          <img src="images/post/2.jpg">
        </div>
        <div class="text"><a href="item_topic.php">การปรับปรุงดินเปรี้ยวสำหรับการปลูกพืชไร่</a>
          <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
          <span>TAG: <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a></span>
        </div>
      </div>
      <!-- End Topic with picture -->

      <!-- Topic with picture -->
      <div class="row">
        <div class="img">
          <img src="images/post/3.jpg">
        </div>
        <div class="text"><a href="item_topic.php">กรมพัฒนาที่ดิน สานต่อโครงการเกษตรทฤษฎีใหม่ เน้นส่งเสริมเกษตรกรปรับปรุงบำรุงดิน-ลดต้นทุนการผลิต</a>
          <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
          <span>TAG: <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a></span>
        </div>
      </div>
      <!-- End Topic with picture -->

      <!-- Topic with picture -->
      <div class="row">
        <div class="img">
          <img src="images/post/4.jpg">
        </div>
        <div class="text"><a href="item_topic.php">เชิญผู้สนใจเข้าร่วมโครงการ "สารเร่ง พด." จุลินทรีย์ที่มีประสิทธิภาพของกรมพัฒนาที่ดินเพื่อผลิตน้ำหมักชีวภาพและปุ๋ยอินทรีย์</a>
          <span class="post">POST: สมาชิกหมายเลข 7432854938 UPDATE: 15/06/2560</span>
          <span>TAG: <a href="#">ความรู้เกษตร</a> <a href="#">ดินเปรี้ยว</a></span>
        </div>
      </div>
      <!-- End Topic with picture -->

    </div> 

  <!-- End Latest topics ################# -->



  <button type="submit" class="btn btn-block">แสดงเพิ่มเติม</button>


</div><!-- column-left -->


<div class="column-right">
  <?php include 'include_side.php';?>
</div><!-- column-right -->

</div><!-- row -->



<?php include 'include_foot.php';?>
<script>
  breadcrumb('&nbsp;');
  activemenu(0);
</script>

</body>
</html>
