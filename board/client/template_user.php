<?php include 'include_head.php';?>

<!-- Modal -->
<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title" id="myModalLabel">กรมพ้ฒนาที่ดิน long terms & conditions</h3>
      </div>

      <div class="modal-body">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non porttitor velit. Phasellus non scelerisque velit. Aliquam ut sagittis tortor, id commodo leo. Proin non augue pulvinar velit accumsan finibus ac dictum dolor. Suspendisse potenti. Quisque id convallis lectus, non tincidunt risus. Mauris bibendum velit nec malesuada luctus. Phasellus et laoreet orci. Pellentesque ullamcorper mollis imperdiet. Donec eu purus risus. Maecenas in sagittis purus, efficitur pellentesque neque. Duis ornare enim sit amet purus elementum, et lobortis sapien luctus. Vestibulum vestibulum cursus tristique. Duis convallis elementum gravida. Cras vel viverra augue, rhoncus rhoncus est.
        </p>
        <p>Cras enim lectus, ornare in lectus nec, interdum semper turpis. Nam venenatis pulvinar orci id pretium. Ut nec vestibulum massa. Duis facilisis, tellus sit amet bibendum lacinia, nulla ex cursus tortor, pharetra aliquet erat nisl sodales elit. In hac habitasse platea dictumst. Donec imperdiet vel quam ut lobortis. Vivamus in tincidunt ante, et mollis enim. Donec non turpis et dui venenatis feugiat nec vel ligula. Donec id lorem accumsan, fermentum arcu non, sodales quam. Curabitur at dictum eros. Donec fermentum consectetur sapien ac rutrum. Sed sagittis commodo finibus.
        </p>
        <p>Duis accumsan accumsan volutpat. Curabitur ex turpis, accumsan ultricies sagittis vitae, molestie et arcu. Maecenas tristique tellus nunc, ac molestie purus iaculis nec. Praesent et iaculis arcu. Aenean sit amet cursus nisl. Integer vestibulum, tellus sit amet ultricies mollis, dui risus pretium ligula, in semper ligula ante vitae neque. In hac habitasse platea dictumst. Pellentesque purus purus, volutpat quis pretium eu, iaculis et lectus. Ut rhoncus magna mi, ut commodo lorem pulvinar a. Aliquam eget faucibus libero. Nullam sed tellus cursus, ultrices quam a, sollicitudin erat. In pretium condimentum aliquam.
        </p>
        <p>Aliquam elementum lectus non magna fringilla, eget sollicitudin mauris dignissim. Fusce interdum felis ut arcu lacinia, sit amet commodo diam faucibus. Integer sit amet rutrum enim, sit amet consequat dui. Proin et lectus nisl. Mauris non pharetra sapien. Sed lacinia nibh non nibh tempor sodales. Phasellus at pharetra sapien. Proin in ligula vitae enim interdum auctor. Sed auctor bibendum massa, sit amet porta leo volutpat sed. Etiam tempor lacus ut imperdiet hendrerit. Vestibulum volutpat leo elit, ut pulvinar ante suscipit ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum in enim sollicitudin, dictum lectus at, aliquam elit. Nunc vulputate nisl ante, non interdum mauris pellentesque ut. Proin sed turpis vitae felis fermentum dapibus. Integer eget orci magna.
        </p>
        <p>Morbi vehicula, lectus a ornare sollicitudin, elit dui placerat ex, at placerat sem felis eu neque. Quisque facilisis lorem quis quam tempus ornare. In non mauris in nisl pellentesque gravida sed et quam. Morbi id nisl velit. Vivamus sit amet volutpat justo. Nam non ullamcorper turpis. Sed nec mattis velit. Integer arcu tortor, bibendum nec justo non, ornare dictum quam. Aliquam rutrum velit in lorem molestie condimentum. Quisque non hendrerit dolor, quis maximus elit. Etiam odio magna, tempus a commodo nec, volutpat non purus. Nunc tempus velit vel ligula luctus placerat.
        </p>   
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non porttitor velit. Phasellus non scelerisque velit. Aliquam ut sagittis tortor, id commodo leo. Proin non augue pulvinar velit accumsan finibus ac dictum dolor. Suspendisse potenti. Quisque id convallis lectus, non tincidunt risus. Mauris bibendum velit nec malesuada luctus. Phasellus et laoreet orci. Pellentesque ullamcorper mollis imperdiet. Donec eu purus risus. Maecenas in sagittis purus, efficitur pellentesque neque. Duis ornare enim sit amet purus elementum, et lobortis sapien luctus. Vestibulum vestibulum cursus tristique. Duis convallis elementum gravida. Cras vel viverra augue, rhoncus rhoncus est.
        </p>
        <p>Cras enim lectus, ornare in lectus nec, interdum semper turpis. Nam venenatis pulvinar orci id pretium. Ut nec vestibulum massa. Duis facilisis, tellus sit amet bibendum lacinia, nulla ex cursus tortor, pharetra aliquet erat nisl sodales elit. In hac habitasse platea dictumst. Donec imperdiet vel quam ut lobortis. Vivamus in tincidunt ante, et mollis enim. Donec non turpis et dui venenatis feugiat nec vel ligula. Donec id lorem accumsan, fermentum arcu non, sodales quam. Curabitur at dictum eros. Donec fermentum consectetur sapien ac rutrum. Sed sagittis commodo finibus.
        </p>
        <p>Duis accumsan accumsan volutpat. Curabitur ex turpis, accumsan ultricies sagittis vitae, molestie et arcu. Maecenas tristique tellus nunc, ac molestie purus iaculis nec. Praesent et iaculis arcu. Aenean sit amet cursus nisl. Integer vestibulum, tellus sit amet ultricies mollis, dui risus pretium ligula, in semper ligula ante vitae neque. In hac habitasse platea dictumst. Pellentesque purus purus, volutpat quis pretium eu, iaculis et lectus. Ut rhoncus magna mi, ut commodo lorem pulvinar a. Aliquam eget faucibus libero. Nullam sed tellus cursus, ultrices quam a, sollicitudin erat. In pretium condimentum aliquam.
        </p>
        <p>Aliquam elementum lectus non magna fringilla, eget sollicitudin mauris dignissim. Fusce interdum felis ut arcu lacinia, sit amet commodo diam faucibus. Integer sit amet rutrum enim, sit amet consequat dui. Proin et lectus nisl. Mauris non pharetra sapien. Sed lacinia nibh non nibh tempor sodales. Phasellus at pharetra sapien. Proin in ligula vitae enim interdum auctor. Sed auctor bibendum massa, sit amet porta leo volutpat sed. Etiam tempor lacus ut imperdiet hendrerit. Vestibulum volutpat leo elit, ut pulvinar ante suscipit ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum in enim sollicitudin, dictum lectus at, aliquam elit. Nunc vulputate nisl ante, non interdum mauris pellentesque ut. Proin sed turpis vitae felis fermentum dapibus. Integer eget orci magna.
        </p>
        <p>Morbi vehicula, lectus a ornare sollicitudin, elit dui placerat ex, at placerat sem felis eu neque. Quisque facilisis lorem quis quam tempus ornare. In non mauris in nisl pellentesque gravida sed et quam. Morbi id nisl velit. Vivamus sit amet volutpat justo. Nam non ullamcorper turpis. Sed nec mattis velit. Integer arcu tortor, bibendum nec justo non, ornare dictum quam. Aliquam rutrum velit in lorem molestie condimentum. Quisque non hendrerit dolor, quis maximus elit. Etiam odio magna, tempus a commodo nec, volutpat non purus. Nunc tempus velit vel ligula luctus placerat.
        </p>   
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In non porttitor velit. Phasellus non scelerisque velit. Aliquam ut sagittis tortor, id commodo leo. Proin non augue pulvinar velit accumsan finibus ac dictum dolor. Suspendisse potenti. Quisque id convallis lectus, non tincidunt risus. Mauris bibendum velit nec malesuada luctus. Phasellus et laoreet orci. Pellentesque ullamcorper mollis imperdiet. Donec eu purus risus. Maecenas in sagittis purus, efficitur pellentesque neque. Duis ornare enim sit amet purus elementum, et lobortis sapien luctus. Vestibulum vestibulum cursus tristique. Duis convallis elementum gravida. Cras vel viverra augue, rhoncus rhoncus est.
        </p>
        <p>Cras enim lectus, ornare in lectus nec, interdum semper turpis. Nam venenatis pulvinar orci id pretium. Ut nec vestibulum massa. Duis facilisis, tellus sit amet bibendum lacinia, nulla ex cursus tortor, pharetra aliquet erat nisl sodales elit. In hac habitasse platea dictumst. Donec imperdiet vel quam ut lobortis. Vivamus in tincidunt ante, et mollis enim. Donec non turpis et dui venenatis feugiat nec vel ligula. Donec id lorem accumsan, fermentum arcu non, sodales quam. Curabitur at dictum eros. Donec fermentum consectetur sapien ac rutrum. Sed sagittis commodo finibus.
        </p>
        <p>Duis accumsan accumsan volutpat. Curabitur ex turpis, accumsan ultricies sagittis vitae, molestie et arcu. Maecenas tristique tellus nunc, ac molestie purus iaculis nec. Praesent et iaculis arcu. Aenean sit amet cursus nisl. Integer vestibulum, tellus sit amet ultricies mollis, dui risus pretium ligula, in semper ligula ante vitae neque. In hac habitasse platea dictumst. Pellentesque purus purus, volutpat quis pretium eu, iaculis et lectus. Ut rhoncus magna mi, ut commodo lorem pulvinar a. Aliquam eget faucibus libero. Nullam sed tellus cursus, ultrices quam a, sollicitudin erat. In pretium condimentum aliquam.
        </p>
        <p>Aliquam elementum lectus non magna fringilla, eget sollicitudin mauris dignissim. Fusce interdum felis ut arcu lacinia, sit amet commodo diam faucibus. Integer sit amet rutrum enim, sit amet consequat dui. Proin et lectus nisl. Mauris non pharetra sapien. Sed lacinia nibh non nibh tempor sodales. Phasellus at pharetra sapien. Proin in ligula vitae enim interdum auctor. Sed auctor bibendum massa, sit amet porta leo volutpat sed. Etiam tempor lacus ut imperdiet hendrerit. Vestibulum volutpat leo elit, ut pulvinar ante suscipit ac. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum in enim sollicitudin, dictum lectus at, aliquam elit. Nunc vulputate nisl ante, non interdum mauris pellentesque ut. Proin sed turpis vitae felis fermentum dapibus. Integer eget orci magna.
        </p>
        <p>Morbi vehicula, lectus a ornare sollicitudin, elit dui placerat ex, at placerat sem felis eu neque. Quisque facilisis lorem quis quam tempus ornare. In non mauris in nisl pellentesque gravida sed et quam. Morbi id nisl velit. Vivamus sit amet volutpat justo. Nam non ullamcorper turpis. Sed nec mattis velit. Integer arcu tortor, bibendum nec justo non, ornare dictum quam. Aliquam rutrum velit in lorem molestie condimentum. Quisque non hendrerit dolor, quis maximus elit. Etiam odio magna, tempus a commodo nec, volutpat non purus. Nunc tempus velit vel ligula luctus placerat.
        </p>   

        <!-- Button set ################# -->
        <div class="buttons centerbutton">
          <button type="submit" class="btn" data-dismiss="modal"><i class="fa fa-check" aria-hidden="true"></i>ยอมรับเงื่อนไขการใช้บริการ</button>
          <button type="submit" class="btn inactive" data-dismiss="modal"><i class="fa fa-close" aria-hidden="true"></i>ยกเลิก</button>
        </div>
        <!-- End Button set ################# -->

      </div><!-- modal-body -->

      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->

    </div>
  </div>
</div>

<div class="row">

  <div class="column-left">

    <!-- Registration ################# -->
    <h3 class="boxtitle">สมัครสมาชิก</h3>

    <form action="template_home.php">
      <div class="form-group">
        <label for="exampleInputEmail1">Email address *</label>
        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword1">Password *</label>  รหัสผ่าน unliturgical inquisitorial provably
        <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="exampleInputPassword2">Re-Password *</label>
        <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="text1">Name *</label>
        <input type="text" class="form-control" id="text1" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="text1">Lastname *</label> toplighted
        <input type="text" class="form-control" id="text1" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="text1">Handle</label>
        <input type="text" class="form-control" id="text1" placeholder="Password">
      </div>
      <div class="form-group">
        <label for="exampleInputFile">Picture</label>
        <img class="avatar" src="images/post/7.jpg">
        <input type="file" id="exampleInputFile">
        <p>Chlorion locomotive messaline Lasi unliturgical inquisitorial provably toplighted</p>
      </div>
      <div class="form-group">
        <hr>
        <p>
          กรมพ้ฒนาที่ดิน terms & conditions recarbonate pseudodemocratic supervolition<br>
          recarbonate pseudodemocratic supervolition Chlorion locomotive messaline Lasi unliturgical inquisitorial provably toplighted <a href="#">รายละเอียด</a>
        </p>
      </div>
      <div class="checkbox">
        <label>
          <input type="checkbox"> Agree terms/conditions pseudodemocratic supervolition Chlorion locomotive messaline Lasi unliturgical inquisitorial provably toplighted
        </label>
      </div>
      <div class="form-group">
        <label for="text1">Handle</label>
        <div class="g-recaptcha" data-sitekey="6LeXsygUAAAAABeyCL4ZiZZPJ4UPPJI-H_N5mD6q"></div>
      </div>
      <button type="button" class="btn btn-block" data-toggle="modal" data-target="#myModal">สมัครสมาชิก</button>
    </form>
    <!-- End Registration ################# -->

  </div><!-- column-left -->


  <div class="column-right">

    <!-- Login box ################# -->
    <div class="box gr space_b">
      <h3 class="space_b">เข้าสู่ระบบ</h3>

      <form action="template_home.php">
        <div class="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox"> Remember me recarbonate pseudodemocratic supervolition Chlorion locomotive messaline Lasi unliturgical inquisitorial provably toplighted
          </label>
        </div>
        <button type="submit" class="btn btn-block">เข้าสู่ระบบ</button>
      </form>

      <div class="space_t">
        <p><a href="#">ลืมรหัสผ่าน</a></p>
      </div>
    </div><!-- box gr -->
    <!-- End Login box ################# -->

  </div><!-- column-right -->

</div><!-- row -->



<?php include 'include_foot.php';?>
<script>
  breadcrumb('สมัครสมาชิก', ['หน้าแรก']);
  activemenu(3);
</script>
</body>
</html>
