<?php include 'include_head.php';?>


<div class="row">

  <div class="column-left">

    <!-- New topic ################# -->
    <div id="box_newpost">

      <h3 class="boxtitle">ตั้งกระทู้</h3>
      <form class="form-horizontal">
        <div class="form-group form-group">
          <label class="col-sm-2 control-label" for="topic_name">หัวข้อกระทู้ *</label>
          <div class="col-sm-10">
            <input class="form-control" type="text" id="topic_name" placeholder="">
          </div>
        </div>
        <div class="form-group form-group">
          <label class="col-sm-2 control-label" for="room_id">เลือกห้อง *</label>
          <div class="col-sm-10">
            <select class="form-control" id="room_id">
              <option>ข้อมูลดิน</option>
              <option>ข้อมูลการใช้ที่ดิน</option>
              <option>ข้อมูลปรับปรุงบำรุงดิน</option>
              <option>ข้อมูลอนุรักษ์ดินและน้ำ</option>
              <option>ข้อมูลการวิเคราะห์ดิน</option>
            </select>
          </div>
        </div>
        <div class="form-group form-group">
          <label class="col-sm-2 control-label" for="room_id">tags *</label>
          <div class="col-sm-10">
            <label class="checkbox-inline" style="padding-left: 30px;">
              <input type="checkbox" id="Checkbox1" value="option1"> ความรู้เกษตร
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> โครงการพระราชดำริ
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> ดินเปรี้ยว      
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> ความรู้เกษตร   
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> หญ้าแฝก         
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> โครงการพระราชดำริ      
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> ดินเปรี้ยว      
            </label>
            <label class="checkbox-inline">
              <input type="checkbox" id="Checkbox1" value="option1"> หญ้าแฝก         
            </label>
          </div>
        </div>
        <div class="form-group form-group">
          <label class="col-sm-2 control-label" for="topic_name">รายละเอียด กระทู้ *</label>
          <div class="col-sm-10">
            <textarea class="form-control" rows="5"></textarea>
          </div>
        </div>
        <div class="row space_b">
          <div class="col-sm-10 col-sm-offset-2">
            <input class="btn" type="button" value="ยกเลิก">
            <input class="btn" type="submit" value="ตั้งกระทู้">
          </div>
        </div>
      </form>

    </div>  
    <!-- End New topic ################# -->

              </div><!-- column-left -->


              <div class="column-right">
                <?php include 'include_side.php';?>
              </div><!-- column-right -->

            </div><!-- row -->



            <?php include 'include_foot.php';?>
<script>
  breadcrumb('ตั้งกระทู้', ['หน้าแรก']);
  activemenu(2);
</script>
          </body>
          </html>
