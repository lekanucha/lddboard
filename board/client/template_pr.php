<?php include 'include_head.php';?>


<div class="row">

  <div class="column-left">

    <!-- PR topics ################# -->

    <div id="box_latest">
      <h3 class="boxtitle">ข่าวประชาสัมพันธ์ พด.</h3>

      <!-- News with picture -->
      <div class="row">
        <div class="img">
          <img src="images/post/5.jpg">
        </div>
        <div class="text"><a href="item_news.php">พด. จัดสัมมนาวันแห่งการต่อต้านการแปรสภาพเป็นทะเลทรายปี' 60</a>
        <span>จากการที่กรมพัฒนาที่ดินได้รับมอบหมายจากกระทรวงเกษตรและสหกรณ์ ให้รับผิดชอบในฐานะหน่วยงานผู้ประสานงานระดับชาติ (National Focal Agency) ของอนุสัญญาว่าด้วยการต่อต้านการแปรสภาพเป็นทะเลทราย (United Nations Convention to Combat Desertification : UNCCD) ซึ่งคณะรัฐมนตรีได้มีมติเห็นชอบให้ประเทศไทยภาคยานุวัติเข้าเป็นสมาชิกอนุสัญญาสหประชาชาติว่าด้วยการต่อต้านการแปรสภาพเป็นทะเลทราย เมื่อวันที่ 12 ธันวาคม 2543
        <a href="item_news.php">อ่านต่อ...</a>
        </span>
        </div>
      </div>
      <!-- End News with picture -->
      
      <!-- News without picture -->
      <p>
        <a href="item_news.php"><i class="fa fa-bullhorn"></i>ขอเชิญร่วมงาน eGovernment Forum 2017 </a>
        <span>ระหว่างวันที่ 16-17 สิงหาคม 2560 ณ อาคารศูนย์ประชุมวายุภักษ์ ห้องวายุภักษ์ 2-4 ชั้น 4 โรงแรมเซ็นทราศูนย์ราชการและคอนเวนชันเซ็นเตอร์ แจ้งวัฒนะ
        <a href="item_news.php">อ่านต่อ...</a>
        </span>
      </p>
      <!-- End News without picture -->

      <!-- News without picture -->
      <p>
        <a href="item_news.php"><i class="fa fa-bullhorn"></i>พด. ลุยเคลื่อนแผนปฏิรูปภาคเกษตร ระดมสมองจัดการผลิต-ตลาด ยกมาตรฐานเกษตรอินทรีย์</a>
        <span>นายสุรเดช เตียวตระกูล อธิบดีกรมพัฒนาที่ดิน เปิดเผยว่า การปฏิรูปภาคการเกษตร มุ่งเน้นการลดต้นทุนการผลิต เพิ่มผลผลิต และยกระดับมาตรฐานสินค้าเกษตร ตลอดทั้งผลิตสินค้าให้ตรงกับความต้องการของผู้บริโภค ซึ่งปัจจุบันมีแนวโน้มและความต้องการสินค้าที่ปลอดภัยต่อสุขภาพ 
        <a href="item_news.php">อ่านต่อ...</a>
        </span>
      </p>
      <!-- End News without picture -->

      <!-- News with picture -->
      <div class="row">
        <div class="img">
          <img src="images/post/6.jpg">
        </div>
        <div class="text"><a href="item_news.php">พด. แนะเกษตรกรสำรองน้ำช่วงหน้าฝนด้วยบ่อจิ๋ว แก้ปัญหาขาดแคลนน้ำในช่วงฤดูแล้งหน้า</a>
        <span>นายสรเดช เตียวตระกล อธิบดีกรมพัฒนาที่ดิน เปิดเผยว่า พล.อ.ฉัตรชัย สาริกัลยะ รัฐมนตรีว่าการกระทรวงเกษตรและสหกรณ์ ได้มอบหมายให้กรมพัฒนาที่ดินดําเนินโครงการแหล่งน้ำในไร่นานอกเขตชลประทาน หรือ บ่อจิ๋ว 
        <a href="item_news.php">อ่านต่อ...</a>
        </span>
        </div>
      </div>
      <!-- End News with picture -->
      
    </div> 

  <!-- End PR topics ################# -->



  <button type="submit" class="btn btn-block">แสดงเพิ่มเติม</button>


</div><!-- column-left -->


<div class="column-right">
  <?php include 'include_side.php';?>
</div><!-- column-right -->

</div><!-- row -->



<?php include 'include_foot.php';?>
<script>
  breadcrumb('ข่าวประชาสัมพันธ์ พด.', ['หน้าแรก']);
  activemenu(4);
  $("#box_pr").hide();
  // $("#box_tag").removeClass('space_t');
</script>

</body>
</html>
