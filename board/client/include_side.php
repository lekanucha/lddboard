<form id="search" class="form">
  <div class="form-group has-success has-feedback">
    <input type="text" class="form-control" placeholder="ค้นหากระทู้">
    <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
    <span id="inputSuccess4Status" class="sr-only">(success)</span>
  </div>
</form>


<!-- USER box ################# -->
<div id="box_user" class="box gr space_b">
  <h3>นายหมอดิน อาสาพัฒนาที่ดินปุ๋ยอินทรีย์</h3>
<div class="row">
<div class="col-xs-6 userinfo">
  <p>สมาชิกหมายเลข 5748392389</p>
  <img class="avatar" src="images/post/7.jpg">
</div>
<div class="col-xs-6 userbutton">
  <button class="btn btn-block">กระทู้ที่ตั้ง <i class="fa fa-comment"></i></button>
  <button class="btn btn-block">กระทู้ที่ตอบ <i class="fa fa-comments"></i></button>
  <button class="btn btn-block">แก้ไขข้อมูล <i class="fa fa-edit"></i></button>
  <button class="btn btn-block">วิธีใช้ PM <i class="fa fa-envelope"></i></button>
  <button class="btn btn-block">ออกจากระบบ <i class="fa fa-sign-out"></i></button>
</div>

</div>
</div><!-- box gr -->
<!-- End USER box ################# -->


<!-- PR box ################# -->
<div id="box_pr" class="box gr space_b">
  <h3 class="space_b">ข่าวประชาสัมพันธ์ พด.</h3>
  <ul class="topiclist">
    <li><a href="#"><i class="fa fa-bullhorn"></i>เชิญผู้สนใจเข้าร่วมโครงการ "สารเร่ง พด." จุลินทรีย์ที่มีประสิทธิภาพของกรมพัฒนาที่ดินเพื่อผลิตน้ำหมักชีวภาพและปุ๋ยอินทรีย์
    </a></li>
    <li><a href="#"><i class="fa fa-bullhorn"></i></span>พด. จัดสัมมนาวันแห่งการต่อต้านการแปรสภาพเป็นทะเลทรายปี' 60</a></li>
  </ul>
  <a href="#" class="showall">แสดงทั้งหมด <i class="fa fa-chevron-right"></i></a>

</div><!-- box gr -->
<!-- End PR box ################# -->



<!-- TAG box ################# -->
<div id="box_tag" class="box gr space_b">
  <h3 class="space_b">แท็กหลัก | แท็กฮิต</h3>
  <ul class="topiclist">
    <li><a href="#"><i class="fa fa-tags"></i></span>ดินเปรี้ยว <span class="pull-right">(125 กระทู้)</span>
    </a></li>
    <li><a href="#"><i class="fa fa-tags"></i></span>ความรู้เกษตร <span class="pull-right">(82 กระทู้)</span>
    </a></li>
    <li><a href="#"><i class="fa fa-tags"></i></span>หญ้าแฝก <span class="pull-right">(78 กระทู้)</span>
    </a></li>
    <li><a href="#"><i class="fa fa-tags"></i></span>โครงการพระราชดำริ <span class="pull-right">(46 กระทู้)</span>
    </a></li>
  </ul>
  <a href="#" class="showall">แสดงทั้งหมด <i class="fa fa-chevron-right"></i></a>

</div><!-- box gr -->
<!-- End TAG box ################# -->

<!-- Icon box ################# -->
<div id="box_icon" class="box bk">
<!-- <img src="images/title_icon.png"><i class="fa fa-2x fa-tags"></i> -->
<div class="icon" id="ldd"></div>
<div class="icon" id="fb"></div>
<div class="icon" id="rss"></div>
<div class="icon" id="yt"></div>
<div class="icon" id="mail"></div>
<!-- <div class="icon" id="line"></div> -->
</div>
<!-- End Icon box ################# -->


<!-- Suggested box ################# -->
<div id="box_suggest" class="box gr space_b">
  <h3 class="space_b">กระทู้ที่คุณอาจสนใจ</h3>
  <ul class="topiclist">
    <li><a href="#"><i class="fa fa-comments"></i>สารอาหารของพืชกับธาตุอาหารของพืชเหมือนหรือแตกต่างกันอย่างไร
    </a></li>
    <li><a href="#"><i class="fa fa-comments"></i>มาทำฮอร์โมนกล้วยอ่อนกัน</a></li>
    <li><a href="#"><i class="fa fa-comments"></i>ปลูกผักไฮโดรโปนิค ถ้าไม่ใช้ปุ๋ย A B ใช้อะไรแทนได้มั้ย</a></li>
  </ul>
  <a href="#" class="showall">แสดงทั้งหมด <i class="fa fa-chevron-right"></i></a>

</div><!-- box gr -->
<!-- End Suggested box ################# -->


<!-- Room selection box ################# -->
<div id="box_roomselect" class="box gr space_b">
  <h3 class="space_b">เลือกห้อง</h3>
  <div id="sideForumMenu" class="btn-group-vertical" role="group">
  </div>
  <!-- 
    <button type="button" class="btn btn-block btn-default">sadsa2</button>
    <button type="button" class="btn btn-block btn-default">sadsa2</button>
    <button type="button" class="btn btn-block btn-default">sadsa2</button> -->

</div><!-- box gr -->
<!-- End Room selection box ################# -->
