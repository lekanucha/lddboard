
</div><!-- container -->

<div class="push"></div>


    <footer class="footer">
      <div class="container">
        <p><span>กรมพัฒนาที่ดิน กระทรวงเกษตรและสหกรณ์</span><br>
        ©2017 Land Development Department 2003/61 ถนนพหลโยธิน แขวงลาดยาว เขตจตุจักร กรุงเทพฯ 10900
Call Center โทร. 1760 e-Mail : cit_1@ldd.go.th</p>
      </div>
    </footer>
    <!-- Bootstrap JavaScript -->
    <!-- 
     -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
     <!-- 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
      -->
    <script src="scripts/jquery-ui.min.js"></script>
    <script src="scripts/jquery.jqplot.min.js"></script>
    <script src="scripts/jqplot.barRenderer.js"></script>
    <script src="scripts/jqplot.categoryAxisRenderer.js"></script>

<!-- 
    <script src="scripts/jquery.sparkline.min.js"></script>
 -->

<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="scripts/excanvas.js"></script><![endif]-->


    <script src="scripts/bootstrap.min.js"></script>
    <script src="scripts/bootstrap-treeview.min.js"></script>
    <script src="scripts/bootstrap-datepicker.min.js"></script>
    <script src="scripts/bootstrap-datepicker.th.min.js"></script>

    <!-- 
    <script src="scripts/bootstrap-datepicker.js"></script>
 -->

<script>

    
    function activemenu(id){
      $('#mainmenu a').removeClass('active');
      $('#mainmenu a:eq('+id+')').addClass('active');
    }
    function breadcrumb(page, items){
      var breadcrumb = document.getElementById("breadcrumb");
      breadcrumb.innerHTML = "";
      if(items){
        items.forEach(function(element) {
          breadcrumb.innerHTML = breadcrumb.innerHTML + '<a href="#">' + element + '</a> <i class="fa fa-chevron-right"></i> ';
        });
      }
      breadcrumb.innerHTML = breadcrumb.innerHTML + '<span>' + page + '</span';
    }

    var forums = [
    {link: 0, name:"ข้อมูลดิน"},
    {link: 0, name:"ข้อมูลการใช้ที่ดิน"},
    {link: 0, name:"ข้อมูลปรับปรุงบำรุงดิน"},
    {link: 0, name:"ข้อมูลอนุรักษ์ดินและน้ำ"},
    {link: 0, name:"ข้อมูลการวิเคราะห์ดิน"},
    {link: 0, name:"ข้อมูลแผนที่"},
    {link: 0, name:"ข้อมูลเกษตรอินทรีย์"},
    {link: 0, name:"ข้อมูลเทคโนโลยีชีวภาพ"},
    {link: 0, name:"ข้อมูลแหล่งน้ำ"},
    {link: 0, name:"ข้อมูลด้านเศรษฐสังคม"}
    ];
    var tags = [
    "ดินเปรี้ยว",
    "ความรู้เกษตร",
    "หญ้าแฝก",
    "โครงการพระราชดำริ"
    ];
    var forumMenu = document.getElementById("forumMenu");
    // console.log(forums);
    var row = 0;
    $.each(forums, function(index, element){
      newrow = index%4;
      if(newrow==0){
        $('#forumMenu').append('<div class="row" id="forumMenuRow'+ row +'"></div>');
      }
      $("#forumMenuRow"+row).append('<div><a href="template_room.php">' + element.name + '</a></div>');
      if(newrow==3){
        row++
      }
      if ($("#sideForumMenu").length){
        $("#sideForumMenu").append('<a href="template_room.php" class="btn btn-default">' + element.name + '</a>');
      }
    });
    $('#collapseMenu a').click(function(){
      // console.log('--');
      // $('#collapseMenu').collapse('hide');
    });

    //Pin button
    $('#pinbutton').click(function(){
      $(this).html($(this).html() == 'เลิกปักหมุด' ? 'ปักหมุด' : 'เลิกปักหมุด');
      $(this).toggleClass('inactive');
      $('#topicpin').toggle();
    })
    $('.reppinbutton').click(function(){
      // console.log($(this).data("target"));
      $(this).html($(this).html() == 'เลิกปักหมุด' ? 'ปักหมุด' : 'เลิกปักหมุด');
      $(this).toggleClass('inactive');
      $($(this).data("target")).toggle();
    })
    /*$('#pinbutton').click(function(){
      $('#pinbutton').toggle(function() {
        $(this).addClass('inactive');
        $(this).text('Before');
      }, function() {
        $(this).removeClass('inactive');
        $(this).text('After');
      });
    });*/
    /*$('#pinbutton').toggle(
      function() {
       $(this).html('<a href="#">After</a>');
     }, 
     function() {
       $(this).html('<a href="#">Before</a>');
     });*/
</script>
